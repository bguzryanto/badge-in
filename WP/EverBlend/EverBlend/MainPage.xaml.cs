﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using EverBlend.Resources;
using Microsoft.Phone.Tasks;
using EverBlend.Models;
using System.Windows.Input;

namespace EverBlend
{
    public partial class MainPage : PhoneApplicationPage
    {

        private CameraCaptureTask _cctask = new CameraCaptureTask();
        private PhotoChooserTask _task = new PhotoChooserTask();
        private PhotoResult _photoResult;


        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void OnLensBlurClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _task.Show();
            _task.Completed += PhotoChooserTask_Completed;
        }

        private void PhotoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                _photoResult = e;

            }
        }

        private void OnForegroundSegmentorClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _task.Show();
            _task.Completed += PhotoChooserTask_Completed;
        }

        private void OnTakePictureClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _cctask.Show();

            _cctask.Completed += PhotoChooserTask_Completed;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (_photoResult != null)
            {
                Photo.OriginalImage = _photoResult.ChosenPhoto;
                Photo.Saved = false;

                _photoResult = null;

                NavigationService.Navigate(new Uri("/Pages/BlendPage.xaml", UriKind.Relative));
            }
        }

        private void onClickAbout(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/AboutPage.xaml", UriKind.Relative));
        }

        private void onClickRateReview(object sender, EventArgs e)
        {
            MarketplaceReviewTask reviewTask = new MarketplaceReviewTask();
            reviewTask.Show();
        }

        
    }
}